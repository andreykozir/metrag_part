$(document).ready(function(){

  var textareas = $("#about_description, #news_description");

  if ( !textareas.length ){return}

  textareas.each(function () {
    window.simplemde = new SimpleMDE({
        element: this,
        toolbar: [

            'bold', 'italic', 'heading', '|',
            {
                name: "Add new line",
                action: drawBRText,
                className: "fa fa-angle-double-right", // Look for a suitable icon
                title: "New line",
            },
            '|',
            'code', 'quote', 'unordered-list', 'ordered-list', '|',
            'link',
            'image',
            'table', '|',
            'preview', 'side-by-side', 'fullscreen', '|',
            '|'
        ]
    });
  })

    function drawBRText(editor)
    {
        var cm = editor.codemirror;
        var output = '';
        var text = cm.getSelection();
        //var text = selectedText || 'placeholder';

        output = text + '(br)';
        cm.replaceSelection(output);
    }
});
