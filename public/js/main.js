(function($){

    /*************************
     basicScript for all pages
     *************************/
    function basicScript() {
        window.windowWidth = $(window).outerWidth();

        /*--toogle slide panel--*/
        var $slide_panel = $("#m-slide-panel");
        if ( $slide_panel.length ){
            $(".m-slide-panel-toogle").on("click", function (){
                $slide_panel.toggleClass("active");
            });
        }

        /*--toogle search form mobile--*/
        var $form_toogle = $("#icon-search, #srch_filter");
        if ( $form_toogle.length ){
            var $header_search_line = $("#header-search-line");

            if ( !$("#srch_page-wrapper" ).length ){//если не страница поиска обьектов
                $form_toogle.on("click", function (){
                    $header_search_line.slideToggle(400);
                });
            }else{
                var $srch_page_wrapper = $("#srch_page-wrapper");
                $form_toogle.on("click", function (){
                    $srch_page_wrapper.toggleClass("srch_form-open");//дополнительно для страница поиска обьектов
                    $header_search_line.slideToggle(400);
                });
            }
        }

        /*--добавление липкого футера, если не IE--*/
        if ( !is_ie_browser && !$("body").hasClass("page-search") ){
            $("#page-wrapper").addClass("sticky-footer");
        }

        /*--размытие для карточек риелторов, если IE--*/
        if ( isIE() ){
            var $img_parent = $('.rieltor_img-blur');
            if ( $img_parent.length ){
                $img_parent.each(function (){
                    var $this = $(this);
                    if ( $this.parents("#m-house-window").length ){return};
                    $this.find("img").css("z-index", "-1")
                    $this.backgroundBlur({
                        imageURL : $this.find("img").attr("src"),
                        blurAmount : 6,
                        imageClass : 'avatar-blur'//'bg-blur'
                    });
                })
            }
        }

        /*--Показать телефон риелтора по клику. Скрывается placeholder элемент и показывается ссылка телефона--*/
        var $rieltor_phone_mask = $(".rieltor_phone_mask");
        if ( $rieltor_phone_mask.length ){
            $rieltor_phone_mask.on("click", function (){
                $(this).parent().addClass("phone-show");
            });
        }

        /*--Страница услуг TABS, если IE--*/
        if ($("#s-services").length){
            $(".services-t_btn").on("click",function (){
                $(this).toggleClass("active").next().slideToggle(400);
            })
        };

        //phones mask
        $(".s-form").find("input[name='phone']").mask("+380 (99) 99 999 99");

        //подогнать изображения обьектов при смене порядка вывода(дада, цена и т.д.)
        $("#adverts_sort").on('change', function() {
            centerImage();
        });


    };
//END basicScript


    /*************************
     searchPageInit переключатель вида страницы поиска
     *************************/
    function searchPageInit(){
        var page_wrap = $("#srch_page-wrapper");
        if( page_wrap.length ){

            var main_wrap = page_wrap.find("#srch_main > .srch_main-wrap");
            var main_and_header = page_wrap.find(".srch_page-h-m");

            //изменить вид (с картой или без)
            $("#sorting_view").on('change', function() {
                if ( this.value == "map" ){
                    main_wrap.addClass("map-show");
                    main_and_header.removeClass("srch_page-h-m-no-map");
                }else if( this.value == "list_map" ){
                    main_wrap.removeClass("map-show");
                    main_and_header.addClass("srch_page-h-m-no-map");
                }
                centerImage();
            });
            //изменить вид ( карта или список)
            $("#btn-toggle-mobile").on('click', function() {
                main_wrap.toggleClass("map-show-mobile");
                centerImage();
            });

        };//if
    };
//END searchPageInit


    /*************************
     sliderOffers - для главной страницы
     *************************/
    function sliderOffers(){
        var best_slider = $("#best-slider");
        if( best_slider.length ){

            best_slider.slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                focusOnSelect: true,
                autoplay: true,
                pauseOnHover: true,
                centerMode: true,
                centerPadding: 0,
                touchThreshold:10,
                autoplaySpeed: 5000,
                responsive: [
                    {
                        breakpoint: 1280,
                        settings: {
                            dots: true,
                            arrows: false,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 820,
                        settings: {
                            dots: true,
                            arrows: false,
                            slidesToShow: 1
                        }
                    }
                ]
            });

        };
    };
//sliderOffers


	/*************************
	  newSlider - для новостроек страницы
	*************************/
    function newSlider(){
        var new_slider = $("#new-slider");
        if( new_slider.length ){

            new_slider.slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                focusOnSelect: true,
                pauseOnHover: true,
                centerMode: true,
                centerPadding: 0,
                touchThreshold:10,
                autoplaySpeed: 5000,
                initialSlide: 1,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });

        };
    };
//newSlider


//-/*******************************/-//
//-/ SEARCH FORM ANIMATIONS FUNCTIONS
    /*************************
     searchFormAnimation
     *************************/
    function searchFormAnimation() {
        var form = $("#search_form");
        if( form.length ){

            var items = form.find(".search_list_i");
            var item_extra = form.find(".search_list_i-extra");
            var items_sub = form.find(".search_list_i-sub");

            var search_form_open = false;//открыто ли подменю

            /*-- клик по элементу списка --*/
            form.find(".search_field").on("click", function (){
                var $this = $(this);
                if ( !$this.parent().hasClass("active") ){//если закрыто - открываем

                    $this.parent().addClass("active");
                    $this.siblings().slideToggle(200);
                    search_form_open = true;

                }else {//если открыто - закрываем //

                    $this.parent().removeClass("active");
                    $this.siblings().fadeOut(0);
                    search_form_open = true;
                }

            });
            /*-- END клик по элементу списка --*/

            /*-- клик по кнопке ок в sub menu --*/
            form.find(".search_button-ok").on("click", function (){
                items.filter(".active").each(function(){
                    var $this = $(this);
                    $this.removeClass("active");
                    $this.find(".search_drop").fadeOut(0);
                });
            });

            /*-- клик по документу и закрытие открытых --*/

            $(document).mousedown(function (e){//

                if ( search_form_open ){

                    if( $(e.target).hasClass("search_button") && $(window).outerWidth() < 768){return};

                    items.filter(".active").each(function(){
                        var $this = $(this);
                        if(
                            !$this.is(e.target )
                            && $this.has(e.target).length === 0
                            && !$(".pac-container").is(e.target )
                            && $(".pac-container").has(e.target).length === 0
                        ) {

                            if ( !items_sub.hasClass("active") ){
                                $this.removeClass("active");
                                $this.find(".search_drop").fadeOut(0);

                                $this.parents(".search_drop").each(function () {
                                    if ( !$(this).hasClass("search_drop-extra") ) {
                                        $(this).fadeOut(0).parent().removeClass("active");
                                    }
                                })

                            }else{
                                items_sub.each(function(){
                                    var $this = $(this);
                                    $this.removeClass("active");
                                    $this.find(".search_drop").fadeOut(0);
                                })
                            }

                            if ( item_extra.hasClass("active") || items.hasClass("active") ){
                                search_form_open = true;
                            }else {
                                search_form_open = false;
                            }

                        }

                    })

                }

            });
            /*-- END клик по документу и закрытие открытых --*/

            /*-- autocomplete input --*/
            var autocomplete_input = $("#autocomplete");

            autocomplete_input.on("click", function (){

                if ( autocomplete_input.is( ":focus" ) ){
                    autocomplete_input.parents(".search_list_i-street").addClass("active");
                }

                search_form_open = true;
            });

            autocomplete_input.focusout(function() {

                search_form_open = true;

                autocomplete_input.parents(".search_list_i-street").removeClass("active");
                var input = this;
                var $this = $(this);

                $drop_box = $(input).parents(".search_drop");//выпадающий элемент

                setTimeout(function($form_items_html){//время чтоб в инпуте появился выбор

                    if ( input.value.length > 10 ){

                        $(input).parent().parent().addClass("activated");
                        formItemText_secondItem($this);

                    }else{

                        $(input).val("");
                        $(input).parent().parent().removeClass("activated");
                        formItemText_secondItem($this);

                    }
                }, 200);//setTimeout
            });

            /*-- END autocomplete input --*/

            /*-- ЖК autocomplete input --*/
            var jk_autocomplete_input = $("#jk-autocomplete");

            jk_autocomplete_input.on("click", function (){

                if ( jk_autocomplete_input.is( ":focus" ) ){
                    jk_autocomplete_input.parents(".search_list_i-jk").addClass("active");
                }

                search_form_open = true;
            });

            jk_autocomplete_input.focusout(function() {

                search_form_open = true;

                jk_autocomplete_input.parents(".search_list_i-jk").removeClass("active");
                var input = this;

                $drop_box = $(input).parents(".search_drop");

                setTimeout(function($form_items_html){

                    if ( input.value.length > 4 ){

                        $(input).parent().parent().addClass("activated");

                    }else{

                        $(input).val("");
                        $(input).parent().parent().removeClass("activated");

                    }
                }, 200);
            });

            /*-- END ЖК autocomplete input --*/


            /*--Слайдер выбора цены--*/
            var slider_el = $("#price_slider");
            var price = slider_el.parents(".price");//Родитель
            var search_list_price = slider_el.parents(".search_list_i-price");//Родитель элемент меню

            var currency_inputs = price.find(".currency_radio-i");
            var currency_uah = price.find(".currency_radio-i[value='1']");//грн
            var currency_usd = price.find(".currency_radio-i[value='2']");//usd

            if ( currency_uah.attr("checked") == 'checked' ){
                var price_slider_min = slider_el.attr("data-min-uah");
                var price_slider_max = slider_el.attr("data-max-uah");
            }else if( currency_usd.attr("checked") == 'checked' ){
                var price_slider_min = slider_el.attr("data-min-usd");
                var price_slider_max = slider_el.attr("data-max-usd");
            }

            var slider_from = price.find("#price_from");//позиции бегунка(число)
            var slider_to = price.find("#price_to");//позиции бегунка(число)

            $("#price_slider").ionRangeSlider({
                type: "double",
                hide_min_max: true,
                hide_from_to: true,
                min: price_slider_min,
                max: price_slider_max,
                from: slider_from.val(),
                to: slider_to.val(),
                onChange: function (data) {
                    slider_from.val(data.from);
                    slider_to.val(data.to);
                    priceParentActivated();
                },
            });
            var price_slider = $("#price_slider").data("ionRangeSlider");

            slider_from.change(function(){
                price_slider.update({
                    from: slider_from.val(),
                });
                priceParentActivated();
            });
            slider_to.change(function(){
                price_slider.update({
                    to: slider_to.val(),
                });
                priceParentActivated();
            });

            currency_inputs.change(function(){

                if (this.value == '1') {//грн
                    price_slider_min = slider_el.attr("data-min-uah");
                    price_slider_max = slider_el.attr("data-max-uah");
                    slider_from.val(slider_el.attr("data-min-uah"));
                    slider_to.val(slider_el.attr("data-max-uah"));
                }
                else if (this.value == '2') {//usd
                    price_slider_min = slider_el.attr("data-min-usd");
                    price_slider_max = slider_el.attr("data-max-usd");
                    slider_from.val(slider_el.attr("data-min-usd"));
                    slider_to.val(slider_el.attr("data-max-usd"));
                }

                price_slider.update({
                    min: price_slider_min,
                    max: price_slider_max,
                    from: slider_from.val(),
                    to: slider_to.val(),
                });

                priceParentActivated();

            });
            function priceParentActivated() {
                search_list_price.addClass("activated");
                search_list_price.find(".search_field_text").html(slider_from.val() + " - " + slider_to.val());
            }
            /*-- END Слайдер выбора цены--*/

            /*-- Вызов записывающих функций --*/
            function formItemText(){
                var form = $("#search_form");

                form.find(".search_input-i").change(function(){
                    var $this = $(this);
                    formItemText_firstItem($this);
                    formItemText_secondItem($this);
                });
                form.find(".search_radio-i").change(function(){
                    var $this = $(this);
                    formItemText_firstItem($this);
                    formItemText_secondItem($this);
                    formShowExtraItems(form, $this);

                });

            };//END formItemText


            formItemText();
            formShowExtraItems(form);
            actionExtraItems(form);
            formOnReadyText(form);
            formRadioUncheckType(form);
            uncheckableNewRooms(form)
            uncheckableRadioAll(form)
            searchDistrictInit(form);
            searchDistrictRun();
            resetFormButton(form);

        }
    }//END searchFormAnimation


    /*************************
     resetFormButton
     *************************/
    function resetFormButton(form) {
        var currency_first = form.find("#currency1"),
            price_parent = form.find(".search_list_i-price");

        form.find("#search_reset").on("click", function () {
            form.find("input[type='radio']").attr('checked', false);
            form.find("input[type='checkbox']").attr('checked', false);
            setTimeout(function() {
                formOnReadyText(form);
                currency_first.click();
                price_parent.removeClass("activated");
                formShowExtraItems(form);
                actionExtraItems(form);
                searchDistrictRun();
            }, 200);
        });
    };
    //END resetFormButton

    /*************************
      searchDistrictInit - отключение метро. связь район-метро.
    *************************/
    function searchDistrictInit() {
        var $form = $("#search_form");
        var $districts = $form.find(".search_list_i-district").find("input");
        var $subways = $form.find(".search_subway-wrap").find("input");
        window.distr_subw_inp = [];

        $districts.each(function (i) {
            var $this = $(this);
            var $this_inx = $this.prop('value');
            $this
            distr_subw_inp[ $this_inx ] = [];
            distr_subw_inp[ $this_inx ][0] = [];
            distr_subw_inp[ $this_inx ][1] = [];
            distr_subw_inp[ $this_inx ][0].push( $this );
        });

        $subways.each(function (i) {
            // this.disabled = true;
            var $this = $(this);
            distr_subw_inp[ $this.attr('data-district_id') ][1].push( $this );
        });

        $districts.on("change", searchDistrictRun);
        // $subways.on("change", searchSubwaysRun);

    };
    function searchDistrictRun() {
        var $counter_disabled_disctricts = 0;
        for (var i = 0; i < distr_subw_inp.length; i++) {

            if ( typeof( distr_subw_inp[i] ) == "undefined" ){continue};

            var indx_lvl = distr_subw_inp[i];
            var distr_lvl = distr_subw_inp[i][0];
            var subwe_lvl = distr_subw_inp[i][1];

            if ( distr_lvl[0].prop('checked') ) {

                for (var ind = 0; ind < subwe_lvl.length; ind++) {
                    subwe_lvl[ind][0].disabled = false;
                }
            }else{
                $counter_disabled_disctricts++;
                for (var ind = 0; ind < subwe_lvl.length; ind++) {
                    if( subwe_lvl[ind].prop('checked') ){
                        subwe_lvl[ind].click();
                    }
                    subwe_lvl[ind][0].disabled = true;
                }
            }
        }
        var length = distr_subw_inp.reduce((sum, el) => {
            return sum + 1
        }, 0)
        if($counter_disabled_disctricts===length){
            var $subways = $("#search_form").find(".search_subway-wrap").find("input");
            $subways.each(function (i) {
                this.disabled = false;

                // console.log("asd")
            });
        }
    };
    // END отключение метро.
    /*************************
     uncheckAllDistricts - удаление выбора всех районов. Сделано для маетро.
     *************************/
    function uncheckAllDistricts() {
        for (var i = 0; i < distr_subw_inp.length; i++) {
            if ( typeof( distr_subw_inp[i] ) == "undefined" ){continue};
                $(distr_subw_inp[i][0][0]).prop('checked',false);
        }
    }
    /*************************
     searchSubwaysRun - событие при выборе метро, для связки с районами
     *************************/
    function searchSubwaysRun(){
        uncheckAllDistricts();
        for (var i = 0; i < distr_subw_inp.length; i++) {
            if ( typeof( distr_subw_inp[i] ) == "undefined" ){continue};

            for (var j = 0; j < distr_subw_inp[i][1].length; j++) {
                if ( typeof( distr_subw_inp[i][1][j] ) == "undefined" ){continue};

                if ( distr_subw_inp[i][1][j].prop('checked')){
                    $(distr_subw_inp[i][0][0]).prop('checked',true);
                    formItemText_firstItem( $(distr_subw_inp[i][0][0]))
                }
            }
        }
    }
    /*************************
      formRadioUncheckType - //Управление радиокнопками формы. Снять выбранное, деактивировать родителя, скрыть доп пункты формы.
    *************************/
    function formRadioUncheckType(form) {

        $.fn.uncheckableRadioType = function () {
            return this.each(function () {
                var radio = this,
                    label = $('label[for="' + radio.id + '"]');
                if (label.length === 0) {
                    label = $(radio).closest("label");
                }
                var label_radio = label.add(radio);
                label_radio.mousedown(function () {
                    $(radio).data('wasChecked', radio.checked);
                });
                label_radio.click(function () {
                    if ($(radio).data('wasChecked')) {
                        radio.checked = false;
                        diactivateParentEl(this);
                        uncheckAllExtraItems(this);
                    }
                });

            });
        };
        function diactivateParentEl($this) {
            var $this = $($this);
            var drop_box = $this.parents(".search_drop")[0];
            $(drop_box).parent().removeClass("activated");
        };
        function uncheckAllExtraItems($this) {
            var $this = $($this);
            if ( !$this.parents(".search_list_i-property").length )return;
            form.find(".search_baths, .search_commercial, .search_stead, .search_new-build, .search_sub-aprtmnt").each(function () {
                $(this).parent().hide().find(".search_input-i:checked").click();
            })
        }
        form.find(".search_list_i-property .search_radio-i").uncheckableRadioType();
        form.find(".search_list_i-action .search_radio-i").uncheckableRadioType();

    };//END formRadioUncheckType

    /*************************
      uncheckableNewRooms - //Управление радиокнопками формы. комнат многостроек
    *************************/
    function uncheckableNewRooms(form) {
        $.fn.uncheckableRadioNew = function () {
            return this.each(function () {
                var radio = this,
                    label = $('label[for="' + radio.id + '"]');
                if (label.length === 0) {
                    label = $(radio).closest("label");
                }
                var label_radio = label.add(radio);
                label_radio.mousedown(function () {
                    $(radio).data('wasChecked', radio.checked);
                });
                label_radio.click(function () {
                    if ($(radio).data('wasChecked')) {
                        radio.checked = false;
                        diactivateParentEl(this);
                    }
                });
            });
        };

        function diactivateParentEl($this) {
            var $this = $($this);
            var drop_box = $this.parents(".search_drop")[0];
            $(drop_box).parent().removeClass("activated");
        };
        
        form.find("#search_drop_rooms .search_input-i").uncheckableRadioNew();
    }
    //END uncheckableNewRooms

    /*************************
      uncheckNewRooms - //Управление радиокнопками формы. комнат многостроек
    *************************/
    function uncheckNewRooms() {
        var radio = $("#search_drop_rooms").find(".search_input-i:checked");
        $("#search_drop_rooms").find(".search_input-i:checked").prop('checked', false);
        formItemText_firstItem(radio)
    }
    //END uncheckNewRooms

    /*************************
      uncheckableRadioAll - //Управление радиокнопками формы. комнат многостроек
    *************************/
    function uncheckableRadioAll(form) {
        $.fn.uncheckableRadio = function () {
            return this.each(function () {
                var radio = this,
                    label = $('label[for="' + radio.id + '"]');
                if (label.length === 0) {
                    label = $(radio).closest("label");
                }
                var label_radio = label.add(radio);
                label_radio.mousedown(function () {
                    $(radio).data('wasChecked', radio.checked);
                });
                label_radio.click(function () {
                    if ($(radio).data('wasChecked')) {
                        radio.checked = false;
                        diactivateParentEl(this);
                    }
                });
            });
        };

        function diactivateParentEl($this) {
            var $this = $($this);
            var drop_box = $this.parents(".search_drop");
            $(drop_box).parent().removeClass("activated");
        };
        
        form.find(".search_ex-wr-days .search_radio-i").uncheckableRadio();
    }
    //END uncheckableRadioAll

    /*************************
     formItemText_firstItem - //Добавляет колличество выбранных пунктов в родительский меню элемент
     *************************/
    function formItemText_firstItem($this){

        var $form_items_html = "";
        $drop_box = $($this.parents(".search_drop")[0]);
        $form_list_item = $drop_box.parent();

        if ( $form_list_item.hasClass("search_list_i-property") ) {
            $drop_box.find("input:checked").each(function(){
                if ( $(this).parents(".search_sub-prop").length ) {return};
                $form_items_html += "<span>" + $(this).siblings("label").html() + "</span>";
            })
        } else {
            $drop_box.find("input:checked").each(function(){
                $form_items_html += "<span>" + $(this).siblings("label").html() + "</span>";
            })
        }

        if ( $form_items_html ){//были ли checked инпуты
            $form_list_item.addClass("activated");//скрыть placeholder и показать текст
            $drop_box.siblings(".search_field").find(".search_field_text").html($form_items_html);

        }else {
            $form_list_item.removeClass("activated");
        }

    }//END formItemText_firstItem

    /*************************
     formItemText_secondItem - //Добавляет колличество выбранных пунктов в родителя родительского меню элемена
     *************************/
    function formItemText_secondItem($this){
        var parent_element;
        var options_number = 0;

        if ($this){//если вызов после события

            if ( $this.parents(".search_list-sub").length ){//если элемент дочернний дочернего списка

                parent_element = $this.parents(".search_list-sub").parents(".search_list_i");

                options_number = parent_element.find("input:checked").length;

                if ( parent_element.find("input:text").val() ){
                    options_number += 1;
                };
                if ( parent_element.find("textarea").val() ){
                    options_number += 1;
                };

                if ( options_number ){//если были выбранны варианты дочернего списка
                    parent_element.addClass("activated");

                    parent_element.children(".search_field").find(".search_field_text").html(
                        parent_element.children(".search_field").find(".search_field_placeholder").html()
                        + " (" + options_number + ")"
                    );

                }else {
                    parent_element.removeClass("activated");
                }

            }

        }
    }//END formItemText_secondItem

    /*************************
     formOnReadyText - вызывается после инициализации всего меню - при вызове записывает все чекнутые и введенные в родителей(form items) и их родителей
     *************************/
    function formOnReadyText(form){

        form.find(".search_input-i, .search_radio-i").each(function () {
            var $this = $(this);
            formItemText_firstItem($this);
        })

        var parent_element = form.find(".search_list-sub").parents(".search_list_i");

        parent_element.each(function () {
            var parent_element = $(this);
            var options_number = parent_element.find("input:checked").length;

            if ( parent_element.find("input:text").val() ){
                options_number += 1;
            };
            if ( parent_element.find("textarea").val() ){
                options_number += 1;
            };

            if ( options_number ){
                parent_element.addClass("activated");

                parent_element.children(".search_field").find(".search_field_text").html(
                    parent_element.children(".search_field").find(".search_field_placeholder").html()
                    + " (" + options_number + ")"
                );

            }else {
                parent_element.removeClass("activated");
            }
        });
    };

    /*************************
      formShowExtraItems - при выборе квартира, дом, участок - меняет дополнительные варианты в дополнительно
    *************************/
    function formShowExtraItems(form, $this) {

        var extra_items = form.find(".search_baths, .search_commercial, .search_stead, .search_new-build, .search_sub-aprtmnt");//.search_rooms, 

        if (!$this){//вызов document on ready, $this не передан
            var $this = form.find(".search_list_i-property").find(".search_radio-i:checked");
        }//else

        if ( $this.length == 0 ) {

            extra_items.each(function (){
                var $this = $(this);
                $this.parent().hide().find(".search_input-i:checked").click();
            })

            return;

        };


        if ( $this.siblings("label").html() == "Квартира" ){

            extra_items.each(function (){
                var $this = $(this);
                if ( $this.hasClass("search_baths") || $this.hasClass("search_sub-aprtmnt")){
                    $this.parent().show(0);
                }else{
                    $this.parent().hide().find(".search_input-i:checked").click();
                }
            })

            uncheckNewRooms();

        }else if ( $this.siblings("label").html() == "Новостройка" ){

            extra_items.each(function (){
                var $this = $(this);
                if ( $this.hasClass("search_baths") || $this.hasClass("search_new-build") ){
                    $this.parent().show(0);
                }else{
                    $this.parent().hide().find(".search_input-i:checked").click();
                }
            })

        }else if ( $this.siblings("label").html() == "Дом" ){

            extra_items.each(function (){
                var $this = $(this);
                if ( $this.hasClass("search_baths") ){
                    $this.parent().show(0);
                }else{
                    $this.parent().hide().find(".search_input-i:checked").click();
                }
            })

            uncheckNewRooms();

        }else if ( $this.siblings("label").html() == "Участок" ){

            extra_items.each(function (){
                var $this = $(this);
                if ( $this.hasClass("search_stead") ){
                    $this.parent().show(0);
                }else{
                    $this.parent().hide().find(".search_input-i:checked").click();
                }
            })

            uncheckNewRooms();

        }else if ( $this.siblings("label").html() == "Коммерческая недвижимость" ){

            extra_items.each(function (){
                var $this = $(this);
                if ( $this.hasClass("search_commercial") ){
                    $this.parent().show(0);
                }else{
                    $this.parent().hide().find(".search_input-i:checked").click();
                }
            })

            uncheckNewRooms();

        }

    };//END formShowExtraItems

    /*************************
      actionExtraItems - при выборе аренда скрыть площадь
    *************************/
    function actionExtraItems(form) {
        var action_radios = form.find(".search_drop-action").find(".search_radio-i");
        var extra_area = form.find(".search_area .search_area_wrap");

        function actionExtraItemsToogle() {
            if ( action_radios.filter(":checked").siblings("label").html() == "Аренда" ){
                extra_area.hide();
            }else{
                extra_area.show();
            }
        };

        actionExtraItemsToogle();

        action_radios.on("click", function () {
            setTimeout(function(){
                actionExtraItemsToogle();
            },100);
        });
    };
    //actionExtraItems

//-/*******************************/-//
//-/ END FORM ANIMATIONS FUNCTIONS /-//
//-/*******************************/-//


    /*************************
     initMapContacts
     *************************/
    window.initMapContacts = function (){

        if ($("#contacts_map").length){

            var element = document.getElementById('contacts_map');
            var options = {
                zoom: 15,
                center: {lat: 49.989712, lng: 36.243114},
            }

            var map = new google.maps.Map(element, options);

            var marker1 = new google.maps.Marker({
                position: {lat: 49.989712, lng: 36.243114},
                map: map,
                icon: "img/contacts-map-icon.png",
            });


        }
    }
    //END initMapContacts


//-/*******************************/-//
//-/ MODAL HOUSE /-//
//-/*******************************/-//
    /*************************
     modalOpenHouse - открытие модального окна обьекта
     *************************/
    function modalOpenHouse(){
        window.m_window = $("#m-house-window");

        if( m_window.length ){

            m_window.iziModal({
                overlayColor: 'rgba(255, 255, 255, 0.45)',
                zindex: 1100,
                openFullscreen: true,
                transitionIn: "fadeInDown",
                onOpening: function(modal){
                    modalSliderHouseImg();
                    adaptive_images(m_house_img_car, m_house_img_nav);
                    modalSliderSimilar();
                    if ( is_ie_browser ){ modalHouseImgBlurIe(); };
                    modalRieltorPhone();
                },
                onOpened: function(){
                    centerImage();
                },
                onClosing: function() {
                    m_house_img_car.slick('unslick');
                    m_house_img_nav.slick('unslick');
                    m_house_similar.slick('unslick');

                    let href = removeParam('realty', window.location.href);
                    window.history.pushState('', '', href);
                }
            });//

            window.m_house_img_car = m_window.find('#m-house_car');
            window.m_house_img_nav = m_window.find('#m-house_nav');
            window.m_house_similar = m_window.find('#similar-slider');
            window.m_house_close_layer = m_window.find(".iziModal-content");

            m_window.find(".m-house_close-btn").on("click", function (){
                m_window.iziModal('close');
            })

            $(document).mouseup(function (e){
                if ( m_house_close_layer.is(e.target ) ){
                    m_window.iziModal('close');
                }
            });

        }
    };
    //modalOpenHouse


    /*************************
     modalSliderHouseImg
     *************************/
    function modalSliderHouseImg(){//слайдер изображений

        m_house_img_car.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            asNavFor: m_house_img_nav,
            autoplay: false,
            infinite: true,
            mobileFirst: true,
            touchThreshold:10,
        });
        m_house_img_nav.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: m_house_img_car,
            dots: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true,
            centerPadding: "0",
            autoplay: false,
            infinite: true,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 1900,
                    settings: {
                        slidesToShow: 9,
                        centerPadding: "30px",
                    }
                },
                {
                    breakpoint: 1750,
                    settings: {
                        slidesToShow: 7,
                        centerPadding: "50px",
                    }
                },
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 5,
                        centerPadding: "100px",
                    }
                },
                {
                    breakpoint: 820,
                    settings: {
                        slidesToShow: 6
                    }
                },
                {
                    breakpoint: 415,
                    settings: {
                        slidesToShow: 7
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 6
                    }
                }

            ]
        });

    };
    //modalSliderHouseImg

    /*************************
     adaptive_images - для модального окна
     *************************/
    function adaptive_images($slider_car) {

        if ( is_ie_browser ){//центрирование для ie браузера при помощи плагина
            setTimeout(function($form_items_html){
                $slider_car.parent().find("img").each(function (){
                    var $this = $(this);
                    $this.parent().imagefill();
                    $this.css({"margin-top":"-3px", "margin-left":"-3px"}); //borders
                });
            },100);


        }else{//центрирование для всех остальных

            $slider_car.find("img").each(function (){

                var $this = $(this);
                var img = this;

                $this.parent().imagesLoaded( function() {

                    if ( img.naturalWidth > img.naturalHeight ){
                        $this.parent().addClass("horizontal_img");
                    }else{
                        $this.parent().addClass("vertical_img");
                    }

                });
            });
        };
    };
    //END adaptive_images


    /*************************
     modalSliderSimilar
     *************************/
    function modalSliderSimilar() {//слайдер похожих обьектов

        m_house_similar.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            focusOnSelect: true,
            autoplay: true,
            pauseOnHover: true,
            centerMode: true,
            centerPadding: 0,
            touchThreshold:10,
            autoplaySpeed: 5000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 820,
                    settings: {
                        dots: true,
                        arrows: false,
                        slidesToShow: 1
                    }
                }
            ]
        });
    };
    //END modalSliderSimilar


    /*************************
     modalHouseImgBlurIe - ТОЛЬКО ДЛЯ IE должно переопределить изображение для размытия
     *************************/
    function modalHouseImgBlurIe(){
        m_window.find('.rieltor_img-blur').each(function (){
            var $this = $(this);
            $this.find("img").css("z-index", "-1")
            $this.backgroundBlur({
                imageURL : $this.find("img").attr("src"),
                blurAmount : 6,
                imageClass : 'avatar-blur'
            });
        })
    }
    //modalHouseImgBlurIe


    /*************************
     modalRieltorPhone - ТОЛЬКО ДЛЯ IE должно переопределить изображение для размытия
     *************************/
    function modalRieltorPhone(){
        m_window.find(".rieltor_phone_mask").on("click", function (){
            $(this).parent().addClass("phone-show");
        });
    }
    //modalRieltorPhone
//-/*******************************/-//
//-/ END MODAL HOUSE /-//
//-/*******************************/-//


    /*************************
     initModalDialogs
     *************************/
    function initModalDialogs(){

        window.m_dialogs = $("#modal_callback, #modal_complaint, #modal_view, #modal_thanks");

        if( m_dialogs.length ){

            m_dialogs.each(function () {
                var $this = $(this);

                $this.iziModal({
                    overlayColor: 'rgba(255, 255, 255, 0.45)',
                    zindex: 1120,//больше чем у модального обьекта недвижимости
                });

                $this.find(".m-dlg_close").on("click", function() {
                    $(this).parents(".modal-form-wrap").iziModal('close');
                })

            });

        };//if
    };
//END initModalDialogs


    /*************************
     formsSubmit
     *************************/
    function formsSubmit(){
        var forms = $("#form-join, #form-question, #form-owners, #m-form-callback, #m-form-complaint, #m-form-view");
        if( forms.length ){


            forms.each(function (){
                var $this = $(this);

                $this.find("textarea").each(function(){
                    autosize(this);
                })

                $this.submit(function(event) {
                    event.preventDefault();

                    $.ajax({
                        type: "POST",
                        url: $this.attr('action'),
                        data: $this.serialize(),
                        dataType: "json",
                        beforeSend: function(jqXHR, settings) {},
                        success: function(data, textStatus, jqXHR) {
                            $this.trigger("reset");

                            $this.trigger("reset");
                            if ( $this.parents(".modal-form-wrap") ){
                                $this.parents(".modal-form-wrap").iziModal('close');
                            }

                            setTimeout(function(){
                                $("#modal_thanks").iziModal('open');
                            }, 500);

                            setTimeout(function(){
                                $("#modal_thanks").iziModal('close');
                            }, 3000);

                        },
                        error: function(jqXHR, textStatus, errorThrown) {},
                        complete: function(jqXHR, textStatus) {},
                    });

                });
            })

        };
    };
//END formsSubmit


    /*************************
     ownersFormAnim
     *************************/
    function ownersFormAnim(){
        var $form = $("#form-owners");
        if ( $form.length ){

            var form_owners_open = false;
            var items = $form.find(".s-form_i-radio");
            var fields = $form.find(".search_field");
            var radios = $form.find(".search_radio-i");

			$form.find("input[name='square']").on("input", function () {
				this.value = this.value.replace (/[A-Za-zА-Яа-яЁё]/g, '');
			});

			$form.find("input[name='price']").on("input", function () {
				this.value = this.value.replace (/[A-Za-zА-Яа-яЁё]/g, '');
			});

            fields.on("click", function (){
                var $this = $(this);

                if ( !$this.parent().hasClass("active") ){

                    $this.parent().addClass("active");
                    $this.siblings().slideToggle(200);

                    form_owners_open = true;
                }else {

                    $this.parent().removeClass("active");
                    $this.siblings().fadeOut(0);
                    form_owners_open = false;
                }
            });

            $(document).mouseup(function (e){

                if ( form_owners_open ){

                    items.filter(".active").each(function(){
                        var $this = $(this);
                        if ( !$this.is(e.target )
                            && $this.has(e.target).length === 0 ) {

                            $this.removeClass("active");
                            $this.find(".search_drop").fadeOut(0);
                        };

                    })
                }
            })

            radios.change(function(){
                var $this = $(this);
                $this.parents(".s-form_i-radio").find(".search_field_placeholder").html("<span>" + $(this).siblings("label").html() + "</span>");
                $this.parents(".s-form_i-radio").addClass("activated");
            });

        }
    };
//END ownersFormAnim


    /*************************
     formsAnimTooltips - модальная форма и другие изменение вида подсказок
     *************************/
    function formsAnimTooltips(){
        if ( $(".m-dlg_form").length ){
            $(".m-dlg_i").each(function (){
                var $this = $(this);

                $this.focusout(function() {

                    if ( $this.val().length > 0 ){
                        $this.parent().addClass("active");
                    }else{
                        $this.parent().removeClass("active");
                    }
                });
            })
        };
    }
//END formsAnimTooltips


    /*************************
     Поиск IE браузеров
     *************************/
    function isIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE '); // IE 10 or older
        var trident = ua.indexOf('Trident/'); //IE 11

        return (msie > 0 || trident > 0);
    }

    var is_ie_browser = isIE();
//END Поиск браузеров


    $(document).ready(function(){
        basicScript();
        searchFormAnimation();
        searchPageInit();
        sliderOffers();
        newSlider();
        initModalDialogs();
        ownersFormAnim();

        centerImage();

    });

    $(window).on( "load", function(){
        initMapContacts();
        formsAnimTooltips();
        reviewsMasonry();
    });

    $(window).on( "resize", function(){
        centerImage(100);
    });


    $( "body" ).on( "click", ".house_img-wrap, .realty_similar, #run_open_modal", function() {

        $('#m-house-window').iziModal('destroy');

        let id = $(this).data('id');

        if(id) {
            let url = updateURLParameter(window.location.href, 'realty', id);
            window.history.pushState('', '', url);
        }

        $.ajax({
            type: "GET",
            url: "/realty/" + id + "/card",
            success: function(msg){

                $('#modal-popup').html(msg);

                modalOpenHouse();
                $('#izi_modal_open_trigger').trigger('click');
            }
        });
    });

})(jQuery);

/*************************
 centerImage - обьекта недвижимости.
 *************************/
    function centerImage(time){
        if (!time){//при ресайзе нужна отсрочка
            var time = 0;
        }

        setTimeout(function(){

            $(".house_img").each(function () {
                var $this = $(this);
                var width = $this.innerWidth();
                $this.css("height", width / 1.58 + "px");
            })

        }, time);

    };
//END centerImage

/*************************
 reviewsMasonry
 *************************/
function reviewsMasonry() {

    var container = document.querySelector('.s-reviews .row');

    if ( container ) {
        var grid_sizer = document.querySelector('.s-reviews .row >div:first-of-type');

        var msnry = new Masonry( container, {
            columnWidth: grid_sizer,
            percentPosition: true,
        });

        $(window).on( "load", function(){
            msnry.layout();//обновляем. неоходимо(бывает не успевает контент)
        });

    };
};
//END reviewsMasonry


function updateURLParameter(url, param, paramVal){
    let newAdditionalURL = "";
    let tempArray = url.split("?");
    let baseURL = tempArray[0];
    let additionalURL = tempArray[1];
    let temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (let i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    let rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}
function removeParam(key, sourceURL) {
    //alert(key);
    //alert('here');
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
function getParam(param){
    var qs = window.location.search.substring(1).split('&');
    var qsp;
    for (p in qs){
        qsp = qs[p].split('=');
        if (qsp[0] == param) return qsp[1];
    }
    return null;
}
