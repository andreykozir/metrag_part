import Vue from './../../node_modules/vue/dist/vue.js'
import hello from './components/hello.vue'

// new Vue({
//     el: "#app",
//     components: {
//         hello: hello
//     }
// })

new Vue({
    el: '#blog-post-demo',
    data: {
        posts: [
            { id: 1, title: 'My journey with Vue' },
            { id: 2, title: 'Blogging with Vue' },
            { id: 3, title: 'Why Vue is so fun' }
        ]
    },
    hello: function() {
        alert('1234');
    }
})


