$('.news_sorting_select').change(function (event) {
    let days = $(event.target).val();

    let url = new URL(window.location.href);
    let page = url.searchParams.get("page");

    let urlStr = '?days=' + days;

    window.location.href = urlStr;
});

$('#adverts_sort').change(function (event) {
    let sortVal = $(event.target).val();

    let url = updateURLParameter(window.location.href, 'sort', sortVal);
    url = updateURLParameter(url, 'page', 1);
    window.history.pushState('', '', url);
    loadSearchedRealty();

    $('body').find('#page_pagination .first').click();
});

$('body').on('click', '#show_on_map', function () {
    showOnMap();
    $(".iziModal-wrap").stop().animate({scrollTop: $(".m-house").outerHeight() }, 400, 'swing');
});

function showOnMap() {

    $('#realty_map').toggleClass('hide');

    let lat = parseFloat($('#show_on_map').attr('data-lat'));
    let lng = parseFloat($('#show_on_map').attr('data-lng'));


    let uluru = {lat: lat, lng: lng};

    let map = new google.maps.Map(document.getElementById('realty_map'), {zoom: 13, center: uluru});

    let marker = new google.maps.Marker({position: uluru, map: map});
}


let searchData;

function loadSearchedRealty() {
    $.ajax({
        method: "GET",
        url: "/search-realties" + window.location.search,
        async: false,
        success: function (data) {
            searchData = data;


            if (data.totalCount === 0) {
                $('.srch_main-content-wrap').css('width', '100%');
            } else {

                paginationInit(searchData.count_pages, loadSearchedRealty, true);
            }

            loadCoordinates(searchData.coordinates, searchData.totalCount);

            $('#count_advert').text(data.totalCount);
            $('#srch_row-content').html(data.html);
        }
    })
}

$(function () {
    if (!$('#srch_page-wrapper').length) {
        return;
    }

    loadSearchedRealty();


});


function getPage() {
    let currentPage = new URL(window.location.href).searchParams.get("page");
    return currentPage === null ? 1 : parseInt(currentPage);
}

let agentRealties;

function loadAgentRealty() {
    let agent_id = $('#agent-realties').data('agent_id');

    $.ajax({
        method: "GET",
        url: "/search-realties?agent=" + agent_id + '&page=' + getPage(),
        async: false,
        success: function (data) {
            agentRealties = data;

            $('#agent-realties').html(data.html);

            if(!data.totalCount) {

                $('#pagination-row').hide();
            }
        }
    })
}

$(function () {
    if (!$('.page-rieltor-card').length) {
        return;
    }

    loadAgentRealty();

    paginationInit(agentRealties.count_pages, loadAgentRealty, true);

});

function addMarker(coord) {
    let icons = {
        type1: {
            icon: 'img/1.png'
        },
        type2: {
            icon: 'img/1.png'
        },
        type3: {
            icon: 'img/3.png'
        },
        type4: {
            icon: 'img/4.png'
        },
        type5: {
            icon: 'img/2.png'
        }
    };

    let marker = new google.maps.Marker({
        position: new google.maps.LatLng(coord.lat, coord.lng),
        map: map,
        title: '',
        icon: icons[coord.type].icon
    });

    infoWindow = new google.maps.InfoWindow();

    var isApartment = coord.type === 'type1' || coord.type === 'type2'|| coord.type === 'type4';
    var isHouse = coord.type === 'type5';
    var isArea = coord.type === 'type3';

    var squareApartmentText = isApartment ? '<p class="realty-map-content-wrap-item">' + coord.square + '/' + coord.square_living + '/' + coord.square_kitchen + 'м²' + '</p>' : '';

    var squareHouseText = (isHouse === true && (coord.square !== null && coord.square > 0)) ? '<p class="realty-map-content-wrap-item">Дом: ' + coord.square + 'м²</p>': '';
    var squareHousePlot = (isHouse === true && (coord.square_plot !== null && coord.square_plot > 0)) ? '<p class="realty-map-content-wrap-item">Участок: ' + coord.square_plot + 'м²</p>': '';
    var squareAreaText = (isArea === true && (coord.total_area !== null && coord.total_area > 0)) ? '<p class="realty-map-content-wrap-item">Площадь: ' + coord.total_area + 'м²</p>': '';


    var floorText = isApartment && (coord.floor && coord.floor_all) ? '<p class="realty-map-content-wrap-item">Этаж: ' + coord.floor + '/' + coord.floor_all + '</p>' : '';

    google.maps.event.addListener(marker, "click", function () { showObjectInfo(coord, squareApartmentText, squareHouseText, squareHousePlot, squareAreaText, floorText, marker)} );
    google.maps.event.addListener(marker, "mouseover", function () { showObjectInfo(coord, squareApartmentText, squareHouseText, squareHousePlot, squareAreaText, floorText, marker)} );

}

let map;

function showObjectInfo(coord, squareApartmentText, squareHouseText, squareHousePlot, squareAreaText, floorText, marker)
{

        let contentString = '<div class="infowindow">' +
            '<div class="realty-map-img-wrap">' +
            '<img src="' + coord.img + '">' +
            '</div>' +
            '<div class="realty-map-content-wrap">' +
            '<h3 class="realty-map-content-wrap-item">' + coord.price + '</h3>' +
            squareApartmentText  +
            squareHouseText +
            squareHousePlot +
            squareAreaText +
            floorText +
            '<a href="#" class="house_img-wrap realty-map-content-wrap-item" data-izimodal-open="#m-house-window" data-id="' + coord.id + '">Показать</a>' +
            '</div>' +
            '</div>';

        infoWindow.setContent(contentString);

        infoWindow.open(map, marker);

}

function initMap1() {
    if ($("#map").length){
	    map = new google.maps.Map(document.getElementById('map'), {
	        center: {lat: 49.988358, lng: 36.232845},
	        zoom: 12
	    });
	}
}

function loadCoordinates(coords, totalCount) {
    if(!totalCount) {
        $('#map').hide();
    } else {
        $('#map').show();
    }

    if(coords !==undefined && coords.length) {
        //transform coordinates from street name to lat and long happens in AddressToLatLngHelper::convert
        for (var coord in coords) {
            addMarker(coords[coord]);
        }
    }
}

showRealtyPopup();

function showRealtyPopup() {

    let realty = new URL(window.location.href).searchParams.get("realty");

    $('#run_open_modal').attr('data-id', realty);

    $('#run_open_modal').trigger('click');
}


function paginationInit(totalPages, callback, isInit = false) {

    if (!$("#page_pagination").length) {
        return
    }

    $("#page_pagination").twbsPagination({
        totalPages: totalPages,
        hideOnlyOnePage: true, //This hides all control buttons if it has one page
        first: '&laquo;',
        prev: '&#139;',
        next: '&#155;',
        last: '&raquo;',
        startPage: getPage(),
        onPageClick: function (evt, page) {
            //it's
            if (!isInit) {
                let url = updateURLParameter(window.location.href, 'page', page);
                window.history.pushState('', '', url);
                initScrollToTop();
                centerImage();
                callback()
            }
            isInit = false;
        }
    });
};

function initScrollToTop() {
    if ($('.sorting-line').length) {
        document.getElementsByClassName('sorting-line')[0].scrollIntoView();
    }

    scrollToElement('body', 500);
}

var scrollToElement = function (el, ms) {
    var speed = (ms) ? ms : 600;
    $('html,body').animate({
        scrollTop: $(el).offset().top
    }, speed);
};

window.onpopstate = function () {
    $('#m-house-window').iziModal('close');

    if (!window.location.href.indexOf("realty") == -1) {
        window.location.href = removeParam('realty');
    } else {
        showRealtyPopup();
        //window.location.reload();
    }
};

let reviewsData;

function loadReviews() {

    let matches = /\/agents\/(\d+)\//.exec(window.location.href);
    let agentId = matches !== null ? matches[1] : null;

    $.ajax({
        method: "GET",
        url: "/api/reviews?agent_id=" + agentId + '&page=' + getPage(),
        async: false,
        success: function (data) {
            reviewsData = data;
            renderReviews(data.reviews);
        }
    })
}

$(function () {

    if (!$('.page-reviews').length) {
        return;
    }

    loadReviews();

    paginationInit(reviewsData.count_pages, loadReviews, true);

});

$(function () {

    if (!$('.front-page').length) {
        return;
    }

    $.ajax({
        method: "GET",
        url: "/api/reviews",
        success: function (data) {
            renderReviews([data.reviews[0], data.reviews[1]], true);
        }
    })

});


function renderReviews(reviews, isHome = false) {
    let strReviews = '';

    if (reviews.length) {
        for (var review in reviews) {
            strReviews += renderReview(reviews[review], isHome);
        }
    } else {
        strReviews += renderDefaultReviews();
    }

    $('#reviews').html(strReviews);

    reviewsMasonry();
}

function renderReview(review, isHome) {
    let reviewClasses = isHome ? '' : 'col-12 col-lg-6';
    return '<div class="' + reviewClasses + '">\n' +
        '<div class="reviews_i">\n' +
        '<div class="reviews_header">\n' +
        '<span class="reviews_date">' + review.date + '</span>\n' +
        '<div class="reviews_author">\n' +
        '<span class="reviews_from">Риэлтор:</span>\n' +
        '<h3><a href="/agents/' + review.agent.id + '" class="reviews_name">' + review.agent.fullname + '</a></h3>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div class="reviews_content">' + review.text + '</div>\n' +
        '</div>\n' +
        '</div>';
}

function renderDefaultReviews() {
    return '<div class="reviews_not_found col-md-12">Отзывы не найденны</div>';
}


/**
 *
 */

let livedComplexesData;

function loadLivedComplexes() {
    //let matches = /\/lived-complex\/(\d+)\//.exec(window.location.href);
    $.ajax({
        method: "GET",
        url: '/api/lived-complex/?&page=' + getPage(),
        async: false,
        success: function (data) {
            livedComplexesData = data;
            renderLivedComplexes(data.lived_complexes);
        }
    })
}

$(function () {
    if (!$('.new-buildings').length) {
        return;
    }

    loadLivedComplexes();
    paginationInit(livedComplexesData.count_pages, loadLivedComplexes, true);
});

function renderLivedComplexes(livedComlexes) {
    let str = '';

    if (livedComlexes.length) {
        for (var livedComlex in livedComlexes) {
            str += renderLivedComplex(livedComlexes[livedComlex]);
        }
    } else {
        str = '<div class="newses_not_found">Дома не найденны</div>';
    }

    $('#lived-complexes').html(str);
}

function renderLivedComplex(livedComlex) {
    let metroStr = livedComlex.metro.length === undefined ? '<h4 class="new-b_district"><img src="/img/line' + livedComlex.metro.line_id + '.png">' + livedComlex.metro.name + '</h4>\n' : '';

    return '<div class="col-12 col-md-6 col-lg-4">\n' +
        '    <div class="new-b">\n' +
        '        <a href="/lived-complex/' + livedComlex.id + '" class="new-b_img-wrap">\n' +
        '            <img src="' + livedComlex.img + '" alt="' + livedComlex.name + '">\n' +
        '        </a>\n' +
        '        <div class="new-cont">\n' +
        '            <h3 class="new-b_ttl"><a href="' + livedComlex.id + '">' + livedComlex.name + '</a></h3>\n' + metroStr +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
}

/**
 *
 */
let newsData;

function loadNewses() {
    let days = new URL(window.location.href).searchParams.get("days");
    let daysSearchStr = '';
    if (days) {
        daysSearchStr = "&days=" + days;
    }
    $.ajax({
        method: "GET",
        url: "/api/news?page=" + getPage() + daysSearchStr,
        async: false,
        success: function (data) {
            newsData = data;
            if(data.newses.length) {
                showPagination();
            } else {
                hidePagination();
            }
            renderNewses(data.newses);
        }
    })
}

function hidePagination() {
    $('#page_pagination').hide();
}
function showPagination() {
    $('#page_pagination').show();
}

$(function () {
    if (!$('.page-news').length) {
        return;
    }

    loadNewses();

    paginationInit(newsData.count_pages, loadNewses, true);

});

function renderNewses(newses) {

    let strNewses = '';

    if (newses.length) {
        for (var news in newses) {
            strNewses += renderNews(newses[news]);
        }
    } else {
        strNewses = '<div class="newses_not_found">Новости за указанный период не найденны</div>';

    }

    $('#newses').html(strNewses);
}

function renderNews(news) {
    let maxLength = 112;

    let newsStr = '';
    if (news.description !== null) {
        if (news.description.length > maxLength) {
            newsStr = news.description.slice(0, 110);
            newsStr = newsStr.slice(0, newsStr.lastIndexOf(" ")) + ' …';
        } else {
            newsStr = news.description;
        }
    }


    return '<div class="col-12 col-lg-6">\n' +
        '<div class="news">\n' +
        '<a href="/news/' + news.id + '" class="news_img">\n' +
        '<img src="/' + news.img + '" alt="news">\n' +
        '</a>\n' +
        '<div class="news_content-wrap">\n' +
        '<div class="news_content">\n' +
        '<span class="news_date">' + news.datetime + '</span>\n' +
        '<h3 class="news_title"><a href="/news/' + news.id + '" class="news_title-l">' + news.title + '</a></h3>\n' +
        '<div class="news_text simplemde-content">' + newsStr + '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>';
}

$(function () {
    if (!$('.front-page').length) {
        return;
    }

    loadNewses();

});

$(function () {
    let search = window.location.pathname + window.location.search;
    let urlPart = /\/([a-zA-Z])/.exec(window.location.pathname);
    addClassActiveIfCurrentRout(search);
});

function addClassActiveIfCurrentRout(urlPart) {
    $('#main-menu a').each(function (index, item) {
        if (!urlPart.indexOf($(item).attr('href'))) {
            $(item).addClass('current_page_item');
        }
    });
}

$(function () {
    $('#form-owners, #form-join, #m-form-callback, #m-form-view, #m-form-complaint, #form-question').submit(function (event) {
        event.preventDefault();

        var $this = $(this);

        $.ajax({
            method: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function (data) {
                $this[0].reset();

                $('#modal_callback').iziModal('close');
                $('#modal_view').iziModal('close');
                $('#modal_complaint').iziModal('close');

                //delay 300 for close
                setTimeout(function () {
                    $("#modal_thanks").iziModal('open');
                }, 500);

                //close in 3000
                setTimeout(function () {
                    $("#modal_thanks").iziModal('close');
                }, 3000);
            },
            error: function (jqXHR) {
                let response = jQuery.parseJSON(jqXHR.responseText);
                $(".form-error").show();
                $('.form-error').text(response.error);
                $(".form-error").delay(7000).fadeOut(400);
            },
            beforeSend() {
                $('.form-error').text('');
            }
        })
    });
});

function initAndRunSimpleMde(element, isPreview = true) {
    if (typeof SimpleMDE !== 'function') {
        return;
    }
    var simplemde = new SimpleMDE({
        element: document.getElementById('simplemde-textarea')
    });

    element.html(simplemde.markdown(element.text()));

    if (element.find('code').length) {
        element.find('code').unwrap().replaceWith('<p>' + element.find('code').text().replace('**', '').replace('**', '') + "<br>" + '</p>');
    }

    replaceAllBR(element);
}

function replaceAllBR(element) {
    let elements = element.find('b,p,h1,h2,h3,h4,h5,h6,blockquote p');
    elements.each(function (index, value) {
        $(this).html($(this).text().replace(new RegExp("\\(br\\)", "gm"), "<br><br>"));
    });
}

initAndRunSimpleMde($('.simplemde-content'));

$(function () {
    $('.news .simplemde-content').each(function (i, obj) {
        initAndRunSimpleMde($(obj), true);
        //remove display simplemde for preview of news.
        $(obj).html($(obj).text());
    });
});

var search_drop_rooms = null;

$('.search_sub-pg-link').mouseenter(function () {
    search_drop_rooms = $(this).attr('data-sub_type');
});

$('.search_sub-pg-link').click(function (event) {
    event.preventDefault();
    goToUrlWithSubType(this, $(this).attr('data-sub_type'));
});

$('.input-rooms').on('change', function (event) {
// $('.input-rooms').on('click', function(event) {
    event.preventDefault();
    goToUrlWithSubType(this, search_drop_rooms);
});

function goToUrlWithSubType(element, sub) {
    let params = $(element).closest('#search_form').serialize() + '&sub_type=' + sub;
    let url = 'search?' + params;

    window.location.href = '/' + url;
}


var street_input = $('#autocomplete');
var streets = [];

street_input.autocomplete({
    lookup: streets,
    maxHeight: 300,
    lookupLimit: 100,
    minChars: 0,
    //types: ["address"],
    // noCache: true,
    // preventBadQueries: false,
    appendTo: $("#locationField"),
    onSelect: function(response, originalQuery) {
        alert('here');
    },
});


$(function () {
    /*************************
     initAutocomplete
     *************************/
    function initAutocomplete() {
        if ($("#autocomplete").length) {
            var autocomplete;

            var cityBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(49.990513, 36.230206));
            autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('autocomplete')),
                {
                    bounds: cityBounds,
                    types: ['address'],
                    componentRestrictions: {country: 'ua'}
                });

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace().name;
                var district = getDistrictVal();
                district = district !== '' ? ', ' + district: '';
                var searchStr = 'Харьков' + district + ', ' + place;
                $('#autocomplete').val(searchStr);
            });
        }
    }

    $('#search_form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });


    initAutocomplete();

    function getDistrictVal()
    {
        var $chedkedDistrict = $('.district-input:checked');
        return $chedkedDistrict.closest('li').find('label').text();
    }

    $('#autocomplete').click(function () {

        var $chedkedDistrict = $('.district-input:checked');
        var districtInputName = ', ' + getDistrictVal() + ',';

        //current district is not found in input
        if($('#autocomplete').val().indexOf(districtInputName) < 0) {

            if ($chedkedDistrict.val() === undefined) {
                    $(this).val('Харьков, ');
            } else if ($chedkedDistrict.length === 1) {
                var district = $chedkedDistrict.closest('li').find('label').text();
                $(this).val('Харьков, ' + district + ', ');
            }
        }
    });

    $('.district-label').click(function () {
        var lengthOfCheckedDistricts = $('.district-input:checked').length;

        if ($(this).closest('li').find('input').prop('checked')) {
            --lengthOfCheckedDistricts;
        } else {
            ++lengthOfCheckedDistricts;
        }

        if (lengthOfCheckedDistricts >= 2) {
            disableDistrictAutocomplete(lengthOfCheckedDistricts);
            $('#autocomplete').val('');
        } else {
            $('#autocomplete').prop("disabled", false);
        }

    });

    if ($('.district-input:checked').length >= 2) {
        disableDistrictAutocomplete();
    }

});

function disableDistrictAutocomplete() {
    $('#autocomplete').prop("disabled", true);
}
