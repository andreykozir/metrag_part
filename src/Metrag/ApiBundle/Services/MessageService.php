<?php

namespace App\Metrag\ApiBundle\Services;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MessageService
{
    /** @var SessionInterface */
    private $session;

    //seconds
    const MAX_MESSAGE_TIME = 60;
    const MAX_REQUEST_COUNT = 3;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function canNotWrite()
    {
        $lastRequest = unserialize($this->session->get('last_request'));

        if(empty($lastRequest['date'])) {
            return false;
        }

        return (($lastRequest['date'] + self::MAX_MESSAGE_TIME) > time()) && ($lastRequest['count'] >= self::MAX_REQUEST_COUNT);

    }

    public function increase()
    {
        $lastRequest = unserialize($this->session->get('last_request'));

        //if isset columns and date is more then now
        if(!empty($lastRequest['date']) && !empty($lastRequest['count']) && ($lastRequest['date'] + self::MAX_MESSAGE_TIME) > time()) {
            $lastRequest['count'] += 1;
        } else {
            $lastRequest = [
                'date' => time(),
                'count' => 1
            ];
        }

        $this->session->set('last_request', serialize($lastRequest));
    }

}