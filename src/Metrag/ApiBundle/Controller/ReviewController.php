<?php
// src/Metrag/ApiBundle/Controller/ReviewController.php

namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Transformers\ReviewTransformer;
use App\Metrag\AppBundle\Helpers\PaginationHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Metrag\AppBundle\Entity\Agent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReviewController extends Controller
{
    public function indexAction(Request $request)
    {
        $page = (int)$request->get('page', 1);
        $limit = getenv('REVIEWS_LIMIT');

        $agentId = (int)$request->get('agent_id', null);
        $filters = ['is_showing' => 1];

        if($agentId) {
            $filters['agent'] = $agentId;
        }

        $reviews = $this
            ->getDoctrine()
            ->getRepository('AppBundle:AgentReview')
            ->findBy($filters, ['datetime' => 'DESC'], $limit, PaginationHelper::getOffset($page, $limit));

        $reviewsCount = $this
            ->getDoctrine()
            ->getRepository('AppBundle:AgentReview')
            ->findBy($filters);

        return new JsonResponse([
            'reviews' => (new ReviewTransformer)->transform($reviews),
            'count_pages' => ceil(count($reviewsCount) / $limit),

        ]);

    }
}