<?php

// src/Metrag/ApiBundle/Controller/NewsController.php
namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\AppBundle\Helpers\PaginationHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Metrag\ApiBundle\Transformers\NewsTransformer;

class NewsController extends Controller
{

    public function indexAction(Request $request)
    {
        $newsesLimit = getenv('NEWSES_LIMIT');
        $days =  $request->query->getInt('days');
        $page =  $request->query->getInt('page', 1);


        //dd($page);

        $query = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->getQueryOnDays($days);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $page, $newsesLimit);

        return new JsonResponse([
            'newses' => (new NewsTransformer)->transform($pagination->getItems()),
            'count_pages' => PaginationHelper::getCounPages($pagination->getTotalItemCount(), $newsesLimit),
            'page' => $page
        ]);
    }
}
