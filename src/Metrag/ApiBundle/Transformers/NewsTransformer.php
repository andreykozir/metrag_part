<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\News;
use App\Metrag\AppBundle\Helpers\DateHelper;

class NewsTransformer
{
    public function transform(array $newses): array
    {
        $response = [];
        /** @var News $news */
        foreach($newses as $news) {
            $response[] = [
                'id' => $news->getId(),
                'title' => $news->getTitle(),
                'description' => $news->getDescription(),
                'img' => $news->getImg(),
                'datetime' => DateHelper::toShowing($news->getDatetime()),
            ];
        }

        return $response;
    }
}
