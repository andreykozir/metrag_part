<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\District;

class DefaultTransformer
{
    public function transform(array $objects): array
    {
        $response = [];
        /** @var District $district */
        foreach ($objects as $object) {
            $response[] = [
                'id' => $object->getId(),
                'name' => $object->getName(),
            ];
        }

        return $response;
    }
}