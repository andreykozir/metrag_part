<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\LivedComplexStatic;

class LivedComplexTransformer
{
    public function transform(array $livedComplexes): array
    {
        $response = [];

        /** @var LivedComplexStatic $livedComplexStatic */
        foreach($livedComplexes as $livedComplex) {
            $metro = $livedComplex->getMetro();

            $response[] = [
                'id' => $livedComplex->getId(),
                'name' => $livedComplex->getName(),
                'description' => $livedComplex->getDescription(),
                'img' => $livedComplex->getImg(),

                'metro' => $metro ?
                    [
                        'id' => $metro->getId(),
                        'name' => $metro->getName(),
                        'line_id' => $metro->getLine()->getId(),
                    ] : [],
            ];
        }

        return $response;
    }
}
