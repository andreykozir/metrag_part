<?php

namespace App\Metrag\AgentBundle\Services;

use App\Metrag\AppBundle\Entity\Realty;
use Doctrine\Common\Persistence\ObjectManager;

class AgentService
{
    /** @var ObjectManager */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getAgentOfRealty(Realty $realty)
    {
        return $realty->getAgents()->toArray() ?: [$this->getDefaultAgent()];
    }

    private function getDefaultAgent()
    {
        return $this->objectManager
            ->getRepository('AppBundle:Agent')
            ->find(getenv('DEFAULT_AGENT'));
    }
}