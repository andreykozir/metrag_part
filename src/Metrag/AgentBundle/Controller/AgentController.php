<?php

// src/Metrag/AgentBundle/Controller/AgentController.php
namespace App\Metrag\AgentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Metrag\AppBundle\Entity\Agent;

class AgentController extends Controller
{
    public function indexAction()
    {
        $agents = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Agent')
            ->findAll();

        return $this->render('@Agent/agent/index.html.twig', [
            'agents' => $agents
        ]);
    }

    public function showAction(Agent $agent)
    {
        return $this->render('@Agent/agent/show.html.twig', [
            'agent' => $agent,
            'count_reviews' => count($agent->getReviews())
        ]);
    }

    public function reviewsAction()
    {
        return $this->render('@Agent/agent/review.html.twig');
    }

}
