<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\Metro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Metro|null find($id, $lockMode = null, $lockVersion = null)
 * @method Metro|null findOneBy(array $criteria, array $orderBy = null)
 * @method Metro[]    findAll()
 * @method Metro[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetroRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Metro::class);
    }
}
