<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\Type;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Realty|null find($id, $lockMode = null, $lockVersion = null)
 * @method Realty|null findOneBy(array $criteria, array $orderBy = null)
 * @method Realty[]    findAll()
 * @method Realty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealtyRepository extends ServiceEntityRepository
{
    private $sortingColumns = [
        'price' => [
            'sort_type' => 'ASC'
        ],
        'datetime' => [
            'sort_type' => 'DESC'
        ]
    ];

    private const MAX_YEAR = 999999;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Realty::class);
    }

    public function removeWhereIdsNotIn(array $ids): bool
    {
        return (bool)$this->createQueryBuilder('r')
            ->delete()
            ->where('r.id NOT IN(' . implode(',', $ids) . ')')
            ->getQuery()
            ->execute();
    }

    public function getByFiltersAndNotCurrentId(array $filters, int $notCurrentId): array
    {
        $query = $this->createQueryBuilder('r')
            ->where('r.id <> :id')
            ->andWhere('r.is_active = true')
            ->setParameter('id', $notCurrentId);

        foreach ($filters as $key => $filter) {
            $query = $query
                ->andWhere('r.' . $key . ' = ' . $filter);
        }

        return $query
            ->setMaxResults(10)
            ->getQuery()
            ->execute();
    }


    public function getByFiltersQuery(array $filters, array $data): Query
    {
        $query = $this->createQueryBuilder('r');

        if (isset($filters['agent']) && (int)$filters['agent']) {
            $this->addFilterIds($query, $filters, $data);
        }

        $this->addFilter($query, $filters, 'deal_type');
        $this->addFilter($query, $filters, 'bathroom_type', 'array');

        $this->addFilter($query, $filters, 'district', 'array');

        $this->addFilter($query, $filters, 'state');

        //array
        $this->addFilter($query, $filters, 'lived_complex', 'array');
        $this->addFilter($query, $filters, 'id', 'array');
        $this->addFilter($query, $filters, 'type', 'array');
        $this->addFilter($query, $filters, 'metro', 'array');
        $this->addFilter($query, $filters, 'rooms_apartment', 'array');
        $this->addFilter($query, $filters, 'rooms');
        $this->addFilter($query, $filters, 'bathrooms', 'array');
        $this->addFilter($query, $filters, 'sub_type', 'array');

        $this->addFilter($query, $filters, 'price', 'between');
        $this->addFilter($query, $filters, 'square', 'between');

        $filters['built_year_from'] = !empty($filters['built_year_from']) ? $filters['built_year_from'] : 0;
        $filters['built_year_to'] = !empty($filters['built_year_to']) ? $filters['built_year_to'] : 99999;
        $this->addFilter($query, $filters, 'built_year', 'between');

        $this->addFilter($query, $filters, 'days', 'datetime');

        if (isset($filters['street'])) {
            $this->addFilterLike($query, ['street'], $filters['street'], true);
        }

        if (isset($filters['words'])) {
            $this->addFilterLike($query, ['description'], $filters['words']);
        }

        if (isset($filters['sort']) && array_key_exists($filters['sort'], $this->sortingColumns)) {
            $query->orderBy('r.' . $filters['sort'], $this->getSortTypeOfColumn($filters['sort']));
        } else {
            $query->orderBy('r.datetime', 'DESC');
        }

        return $query
            ->andWhere('r.is_active = true')
            ->getQuery();
    }

    private function getStreetNameFromInput(string $street): string
    {
        $streetArray = explode(',', $street);

        //if street name not found
        if(empty($streetArray[1])) {
            return '';
        }

        $streetArray[1];

        $districtsFromText = $this
            ->getEntityManager()
            ->getRepository('AppBundle:District')
            ->findOneBy(['name' => trim($streetArray[1])]);

        return $districtsFromText ? trim($streetArray[2]) : trim($streetArray[1]);
    }

    public function getPriceMinAndMax(): ?array
    {
        $qb = $this->createQueryBuilder('u');

        $qb->select('MAX(u.price) as price_max, MIN(u.price) as price_min');

        return $qb->getQuery()->getSingleResult();
    }

    public function getSquareMinAndMax(): ?array
    {
        $qb = $this->createQueryBuilder('u');

        $qb->select('MIN(u.square) as square_min, MAX(u.square) as square_max');

        return $qb->getQuery()->getSingleResult();
    }

    public function countByColumns(array $status): int
    {
        $result = $this->createQueryBuilder('r')
            ->select('COUNT(r.id) as cnt')
            ->where('r.is_active = true')
            ->andWhere('r.type IN(' . implode(',', $status) . ')')
            ->getQuery()
            ->getSingleResult();

        return $result['cnt'];
    }

    /**
     * Realty to agent. If agent is not exists in agent_realties, add default agent
     * Client add fixes, and filter was rewritten in this class.
     *
     * @param $query
     * @param array $filters
     * @param array $data
     *
     */
    private function addFilterIds(&$query, array &$filters, array $data)
    {
        $agent = intval($filters['agent']) ?? null;

        $sql = 'SELECT ar.realty_id
        FROM agent_realties as ar
        WHERE ar.agent_id = :agent_id';

        try {
            $connection = $this
                ->getEntityManager()
                ->getConnection();
            $stmt = $connection->prepare($sql);
            $stmt->execute(['agent_id' => $agent]);
            $realties = $stmt->fetchAll();

        } catch (\Exception $exception) {
            //TODO handle error
            return [];
        }

        $idsIn = [];

        /** @var $realty Realty */
        foreach ($realties as $realty) {
            $idsIn[] = $realty['realty_id'];
        }

        if ($agent && ((int)$agent) === (int)getenv('DEFAULT_AGENT')) {
            $query->andWhere('r.id NOT IN(:ids_not_in) OR r.id IN(:ids_in)')
                ->setParameters([
                    'ids_not_in' => $data['not_main_agent_realties_ids'],
                    'ids_in' => $idsIn,
                ]);
        } elseif ($agent) {
            $filters['id'] = $idsIn;
        }
    }

    private function getSortTypeOfColumn(string $columnName): string
    {
        return $this->sortingColumns[$columnName]['sort_type'];
    }

    private function getDoctineWhere(bool $or = false): string
    {
        return $or ? 'orWhere' : 'andWhere';
    }

    private function addFilterLike(&$query, array $searchColumns, string $searchText, bool $or = false): void
    {
        if (!$searchText) {
            return;
        }

        $isSetDistrict = (bool)$query->getParameter('district');
        $isSetMetro = (bool)$query->getParameter('metro');

        $doctrineWhere = $this->getDoctineWhere(($isSetDistrict && $or) || $isSetMetro);

        foreach ($searchColumns as $column) {
            $query->$doctrineWhere('r.' . $column . ' LIKE :' . $column)
                ->setParameter($column, '%' . $this->getStreetNameFromInput($searchText) . '%');
        }
    }

    private function addFilter(&$query, array $filters, $key, $type = null, bool $or = false): void
    {
        //it's works for array condition
        $doctrineWhere = $this->getDoctineWhere();

        //if key exists in filters.
        if (!(isset($filters[$key]) || isset($filters[$key . '_from']) || isset($filters[$key . '_to']))) {
            return;
        }

        $additionSql = '';
        $typeOfRealty = isset($filters['type']) ? (int)$filters['type'] : null;
        if ($type === null) {

            if ($key === 'rooms' && $typeOfRealty !== Type::TYPES['building']) {
                return;
            }

            $query->andWhere('r.' . $key . ' = :' . $key . $additionSql)
                ->setParameter($key, $filters[$key]);
        } elseif ($type === 'array') {
            if ($typeOfRealty === Type::TYPES['flat'] && $key === 'rooms_apartment') {
                $query->$doctrineWhere('r.rooms' . ' IN(:' . $key . ')')
                    ->setParameter($key, $filters[$key]);
            } else {
                $query->$doctrineWhere('r.' . $key . ' IN(:' . $key . ')')
                    ->setParameter($key, $filters[$key]);
            }
        } elseif ($type === 'between') {

            //add keys from and to. in form will be the same sufix in input name
            $keyFrom = $key . '_from';
            $keyTo = $key . '_to';

            $query->andWhere('r.' . $key . ' BETWEEN :' . $keyFrom . ' AND :' . $keyTo)
                ->setParameter($keyFrom, $filters[$keyFrom])
                ->setParameter($keyTo, $filters[$keyTo]);

        } elseif ($type === 'datetime') {
            $datetime = new \DateTime();
            $datetime->modify('-' . (int)$filters[$key] . '  days');

            $query
                ->andWhere('r.datetime > :datetime')
                ->setParameter('datetime', $datetime);
        }
    }
}
