<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\BathroomType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BathroomType|null find($id, $lockMode = null, $lockVersion = null)
 * @method BathroomType|null findOneBy(array $criteria, array $orderBy = null)
 * @method BathroomType[]    findAll()
 * @method BathroomType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BathroomTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BathroomType::class);
    }

}
