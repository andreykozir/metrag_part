<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\ServiceItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ServiceItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceItem[]    findAll()
 * @method ServiceItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ServiceItem::class);
    }

}
