<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CurrentUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrentUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrentUser[]    findAll()
 * @method CurrentUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }
}
