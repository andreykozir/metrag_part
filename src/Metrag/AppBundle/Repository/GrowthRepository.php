<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\Growth;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Growth|null find($id, $lockMode = null, $lockVersion = null)
 * @method Growth|null findOneBy(array $criteria, array $orderBy = null)
 * @method Growth[]    findAll()
 * @method Growth[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrowthRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Growth::class);
    }

}
