<?php

namespace App\Metrag\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table(name="complaints")
 * @ORM\Entity
 */
class Complaint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string")
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="contacts", type="text")
     */
    private $contacts;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="complaint_on_id", type="string")
     */
    private $complaint_on_id;

    /**
     * @var integer(1 - realty. )
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContacts(): ?string
    {
        return $this->contacts;
    }

    public function setContacts(string $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getComplaintOnId(): ?string
    {
        return $this->complaint_on_id;
    }

    public function setComplaintOnId(string $complaint_on_id): self
    {
        $this->complaint_on_id = $complaint_on_id;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(?string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }
}