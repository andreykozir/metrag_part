<?php

namespace App\Metrag\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Metrag\AppBundle\Services\ImgService;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Agent
 *
 * @ORM\Table(name="agents")
 * @ORM\Entity(repositoryClass="App\Metrag\AppBundle\Repository\AgentRepository")
 */
class Agent
{
    public function __toString()
    {
        return $this->fullname;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="text")
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="numbers", type="json_array")
     */
    private $numbers;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="text", nullable=true)
     */
    private $img;


    /**
     * @ORM\OneToMany(targetEntity="App\Metrag\AppBundle\Entity\AgentReview", mappedBy="agent")
     */
    private $reviews;

   /**
     * Many Realties have Many Agents.
     *
     * @ManyToMany(targetEntity="App\Metrag\AppBundle\Entity\Realty", cascade={"persist", "remove"})
     * @JoinTable(
     *      joinColumns={@JoinColumn(name="agent_id", referencedColumnName="id",  onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="realty_id", referencedColumnName="id",  onDelete="CASCADE")}
     * )
     */
    private $realties;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
        $this->realties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        return $this->id = $id;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getNumbers()
    {
        return $this->numbers;
    }

    public function setNumbers($numbers): self
    {
        $this->numbers = $numbers;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    /**
     * @param string $img
     * @return Agent
     *
     * TODO rewrite to automatically upload file. It's costal, need because in easyadmin can not use path to saving image. vendor libraries do not works.
     */
    public function setImg(string $img = null): self
    {

        if($img) {
            $imgName = ImgService::upload(self::getUploadDir(), $img);
            $this->img = $imgName;
        }

        return $this;
    }

    /**
     * @return Collection|AgentReview[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(AgentReview $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setAgents($this);
        }

        return $this;
    }

    public function removeReview(AgentReview $review): self
    {
        if ($this->reviews->contains($review)) {
            $this->reviews->removeElement($review);
            // set the owning side to null (unless already changed)
            if ($review->getAgents() === $this) {
                $review->setAgents(null);
            }
        }

        return $this;
    }

    public static function getUploadDir()
    {
        return 'agent';
    }

    /**
     * @return Collection|Realty[]
     */
    public function getRealties(): Collection
    {
        return $this->realties;
    }

    public function addRealty(Realty $realty): self
    {
        if (!$this->realties->contains($realty)) {
            $this->realties[] = $realty;
        }

        return $this;
    }

    public function removeRealty(Realty $realty): self
    {
        if ($this->realties->contains($realty)) {
            $this->realties->removeElement($realty);
        }

        return $this;
    }
}