<?php

namespace App\Metrag\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Realty
 *
 * @ORM\Table(name="realties",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="foreign_id_type_deal_type",
 *            columns={"foreign_id", "type_id", "deal_type_id"})
 *    }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Metrag\AppBundle\Repository\RealtyRepository")
 */
class Realty
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="foreign_id", type="integer")
     * @Assert\NotNull()
     */
    private $foreign_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Type", inversedBy="realties")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id" )
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\SubType", inversedBy="realties")
     * @ORM\JoinColumn(name="sub_type_id", referencedColumnName="id" )
     */
    private $sub_type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\District", inversedBy="realties")
     * @ORM\JoinColumn(name="district_id", referencedColumnName="id" )
     */
    private $district;

    /**
     * @var array
     *
     * @ORM\Column(name="coordinates", type="array", nullable=true)
     */
    private $coordinates;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="rooms", type="integer", nullable=false)
     */
    private $rooms;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="integer", nullable=true)
     */
    private $floor;

    /**
     * @var string
     *
     * @ORM\Column(name="floor_all", type="integer", nullable=true)
     */
    private $floor_all;

    /**
     * @var string
     *
     * @ORM\Column(name="square", type="integer", nullable=true)
     */
    private $square;

    /**
     * @var string
     *
     * @ORM\Column(name="square_living", type="integer", nullable=true)
     */
    private $square_living;

    /**
     * @var string
     *
     * @ORM\Column(name="square_kitchen", type="integer", nullable=true)
     */
    private $square_kitchen;

    /**
     * @var string
     *
     * @ORM\Column(name="square_plot", type="integer", nullable=true)
     */
    private $square_plot;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\DealType", inversedBy="realties")
     * @ORM\JoinColumn(name="deal_type_id", referencedColumnName="id")
     */
    private $deal_type;

    /**
     * @var string
     *
     * @ORM\Column(name="is_combined_bathroom", type="boolean", nullable=true)
     */
    private $is_combined_bathroom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\BathroomType", inversedBy="realties")
     * @ORM\JoinColumn(name="bathroom_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $bathroom_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\State", inversedBy="realties")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Layout", inversedBy="realties")
     * @ORM\JoinColumn(name="layout_id", referencedColumnName="id")
     */
    private $layout;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Status", inversedBy="realties")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="built_year", type="integer")
     */
    private $built_year;

    /**
     * @var string
     *
     * @ORM\Column(name="count_balcony", type="integer", nullable=true)
     */
    private $count_balcony;

    /**
     * @var string
     *
     * @ORM\Column(name="count_balcony_glasses", type="integer", nullable=true)
     */
    private $count_balcony_glasses;

    /**
     * @var array
     *
     * @ORM\Column(name="images", type="array", nullable=true)
     */
    private $images;


    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Metro", inversedBy="realties")
     * @ORM\JoinColumn(name="metro_id", referencedColumnName="id", nullable=true)
     */
    private $metro;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="number_bulding", type="string", nullable=true)
     */
    private $number_bulding;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\LivedComplex", inversedBy="realties")
     * @ORM\JoinColumn(name="lived_complex_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $lived_complex;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active = true;

    /**
     * Many Realties have Many Agents.
     *
     * @ManyToMany(targetEntity="App\Metrag\AppBundle\Entity\Agent")
     * @JoinTable(name="agent_realties",
     *      joinColumns={@JoinColumn(name="realty_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="agent_id", referencedColumnName="id")}
     * )
     */
    private $agents;

    public function __construct()
    {
        $this->agents = new ArrayCollection();
    }

    public function __toString(): string
    {
        return 'Type: ' . $this->getType() .  '. Foreign id: ' . $this->getForeignId();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getForeignId(): ?int
    {
        return $this->foreign_id;
    }

    public function setForeignId(int $foreign_id): self
    {
        $this->foreign_id = $foreign_id;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    public function setCoordinates(?array $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRooms(): ?int
    {
        return $this->rooms;
    }

    public function setRooms(?int $rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->floor;
    }

    public function setFloor(?int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function getFloorAll(): ?int
    {
        return $this->floor_all;
    }

    public function setFloorAll(?int $floor_all): self
    {
        $this->floor_all = $floor_all;

        return $this;
    }

    public function getSquare(): ?int
    {
        return $this->square;
    }

    public function setSquare(?int $square): self
    {
        $this->square = $square;

        return $this;
    }

    public function getIsCombinedBathroom(): bool
    {
        return (bool)$this->is_combined_bathroom;
    }


    public function setIsCombinedBathroom(?bool $is_combined_bathroom): self
    {
        $this->is_combined_bathroom = $is_combined_bathroom;

        return $this;
    }

    public function getBathroomType()
    {
        return $this->is_combined_bathroom ? 'совмещенный': 'разделенный';
    }

    public function getBathroomTypeName()
    {
        return $this->bathroom_type ?? null;
    }

//    public function getBathrooms(): ?int
//    {
//        return $this->bathrooms;
//    }
//
//    public function setBathrooms(?int $bathrooms): self
//    {
//        $this->bathrooms = $bathrooms;
//
//        return $this;
//    }

    public function getBuiltYear(): ?int
    {
        return $this->built_year;
    }

    public function setBuiltYear(?int $built_year): self
    {
        $this->built_year = $built_year;

        return $this;
    }

    public function getCountBalcony(): ?int
    {
        return $this->count_balcony;
    }

    public function setCountBalcony(?int $count_balcony): self
    {
        $this->count_balcony = $count_balcony;

        return $this;
    }

    public function getCountBalconyGlasses(): ?int
    {
        return $this->count_balcony_glasses;
    }

    public function setCountBalconyGlasses(?int $count_balcony_glasses): self
    {
        $this->count_balcony_glasses = $count_balcony_glasses;

        return $this;
    }

    public function getImages(): ?array
    {
        $images = [];
        if($this->isFoundImgs()) {
            foreach($this->images as $img) {
                $images[] = getenv('BASE_IMG_PATH') . $img;
                //$img = getenv('BASE_IMG_PATH') . $img ;
            }
        } else {
            $images[] = '/img/default.jpg';
        }

        return $images;
    }

    public function setImages(?array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function getTypeName(): string
    {
        return isset($this->type->name) ? $this->type->name : '';
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }
    public function getDistrictName(): string
    {
        return isset($this->district->name) ? $this->district->name : '';
    }

    public function setDistrict(?District $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getDealType(): ?DealType
    {
        return $this->deal_type;
    }

    public function setDealType(?DealType $deal_type): self
    {
        $this->deal_type = $deal_type;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }
    public function getStateName(): string
    {
        return isset($this->state->name) ? $this->state->name : '';
    }

    public function setState(?State $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getLayout(): ?Layout
    {
        return $this->layout;
    }
    public function getLayoutName(): string
    {
        return isset($this->layout->name) ? $this->layout->name : '';
    }

    public function setLayout(?Layout $layout): self
    {
        $this->layout = $layout;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }
    public function getStatusName(): string
    {

        return !empty($this->status) ? $this->status->getName() : '';
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMetro(): ?Metro
    {
        return $this->metro;
    }
    public function getMetroName(): string
    {
        return isset($this->metro->name) ? $this->metro->name : '';
    }

    public function setMetro(?Metro $metro): self
    {
        $this->metro = $metro;

        return $this;
    }

    public function getRepair(): ?Repair
    {
        return $this->repair;
    }

    public function setRepair(?Repair $repair): self
    {
        $this->repair = $repair;

        return $this;
    }

    public function getFirstImg(): string
    {
        //dd($this->getImages());
        return $this->getImages()[0];
    }
    public function getCountImgs(): string
    {
        return $this->isFoundImgs() ? count($this->images) : 0;
    }
    private function isFoundImgs(): bool
    {
        return $this->images[0] !== null;
    }

    public function getSquareLiving(): ?int
    {
        return $this->square_living;
    }

    public function setSquareLiving(?int $square_living): self
    {
        $this->square_living = $square_living;

        return $this;
    }

    public function getSquareKitchen(): ?int
    {
        return $this->square_kitchen;
    }

    public function setSquareKitchen(?int $square_kitchen): self
    {
        $this->square_kitchen = $square_kitchen;

        return $this;
    }

    public function getSubType(): ?SubType
    {
        return $this->sub_type;
    }

    public function setSubType(?SubType $sub_type): self
    {
        $this->sub_type = $sub_type;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getLivedComplex(): ?LivedComplex
    {
        return $this->lived_complex;
    }
    public function getLivedComplexName(): string
    {
        return isset($this->lived_complex) ? $this->lived_complex->getName() : '';
    }

    public function setLivedComplex(?LivedComplex $lived_complex): self
    {
        $this->lived_complex = $lived_complex;

        return $this;
    }

    /**
     * @return Collection|Agent[]
     */
    public function getAgents(): Collection
    {
        return $this->agents;
    }

    public function addAgent(Agent $agent): self
    {
        if (!$this->agents->contains($agent)) {
            $this->agents[] = $agent;
        }

        return $this;
    }

    public function removeAgent(Agent $agent): self
    {
        if ($this->agents->contains($agent)) {
            $this->agents->removeElement($agent);
        }

        return $this;
    }
    public function removeAllAgents(): self
    {
        $this->agents = [];
        return $this;
    }

    public function setBathroomType(?BathroomType $bathroom_type): self
    {
        $this->bathroom_type = $bathroom_type;

        return $this;
    }

    public function getLastUpdateCoordinates(): ?\DateTimeInterface
    {
        return $this->last_update_coordinates;
    }

    public function setLastUpdateCoordinates(\DateTimeInterface $last_update_coordinates): self
    {
        $this->last_update_coordinates = $last_update_coordinates;

        return $this;
    }

    public function getNumberBulding(): ?string
    {
        return $this->number_bulding;
    }

    public function setNumberBulding(?string $number_bulding): self
    {
        $this->number_bulding = $number_bulding;

        return $this;
    }

    public function getSquarePlot(): ?int
    {
        return $this->square_plot;
    }

    public function setSquarePlot(?int $square_plot): self
    {
        $this->square_plot = $square_plot;

        return $this;
    }
}
