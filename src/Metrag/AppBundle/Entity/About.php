<?php

namespace App\Metrag\AppBundle\Entity;

use App\Metrag\AppBundle\Services\ImgService;
use Doctrine\ORM\Mapping as ORM;
/**
 * Type
 *
 * @ORM\Table(name="static_about")
 * @ORM\Entity
 */
class About
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    /**
     * @param string $img
     * @return About
     *
     * TODO rewrite to automatically upload file. It's costal, need because in easyadmin can not use path to saving image. vendor libraries do not works.
     */
    public function setImg(string $img = null): self
    {
        if($img) {
            $imgName = ImgService::upload($this->getUploadDir(), $img);
            $this->img = $imgName;
        }
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getUploadDir()
    {
        return 'static';
    }
}