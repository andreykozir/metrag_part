<?php

namespace App\Metrag\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * Type
 *
 * @ORM\Table(name="static_services")
 * @ORM\Entity
 */
class Service
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="list_title", type="string")
     */
    private $list_title;

    public function __construct()
    {
        $this->service_items = new ArrayCollection();
    }



    public function __toString(): string
    {
        return 'Id: ' . $this->getId() . '. ' . $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getListTitle(): ?string
    {
        return $this->list_title;
    }

    public function setListTitle(string $list_title): self
    {
        $this->list_title = $list_title;

        return $this;
    }

    /**
     * @return Collection|ServiceItem[]
     */
    public function getServiceItems(): Collection
    {
        return $this->service_items;
    }

    public function addServiceItem(ServiceItem $serviceItem): self
    {
        if (!$this->service_items->contains($serviceItem)) {
            $this->service_items[] = $serviceItem;
            $serviceItem->setServiceId($this);
        }

        return $this;
    }

    public function removeServiceItem(ServiceItem $serviceItem): self
    {
        if ($this->service_items->contains($serviceItem)) {
            $this->service_items->removeElement($serviceItem);
            // set the owning side to null (unless already changed)
            if ($serviceItem->getServiceId() === $this) {
                $serviceItem->setServiceId(null);
            }
        }

        return $this;
    }

}