<?php

// src/Metrag/AppBundle/HtmlController.php

namespace App\Metrag\AppBundle\Controller;

use App\Metrag\AgentBundle\Services\AgentService;
use App\Metrag\ApiBundle\Services\FilterService;
use App\Metrag\ApiBundle\Services\RealtyService;
use App\Metrag\ApiBundle\Transformers\AgentTransformer;
use App\Metrag\ApiBundle\Transformers\DefaultTransformer;
use App\Metrag\ApiBundle\Transformers\LivedComplexListTransformer;
use App\Metrag\ApiBundle\Transformers\RealtyTransformer;
use App\Metrag\AppBundle\Entity\DealType;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\Type;
use App\Metrag\AppBundle\Services\CurrencyService;
use App\Metrag\AppBundle\Transformers\MetroTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Render html blocks. One method - one html block
 *
 * Class HtmlController
 * @package App\Metrag\AppBundle\Controller
 * @TODO split controller
 */
class HtmlController extends Controller
{
    public function renderPhoneAction(string $number)
    {
        return $this->render('@App/html/phones.html.twig', [
            'number' => $number
        ]);
    }

    /**
     * To avoid troubles with reinitializing the slider, the realtor card is moved to the backend.
     *
     * @param Realty $realty
     * @param RealtyService $realtyService
     * @param AgentService $agentService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderCardAction(Realty $realty, RealtyService $realtyService, AgentService $agentService)
    {
        if(!$realty->getIsActive()) {
            throw new NotFoundHttpException('Page not found');
        }

        return $this->render('@App/html/card.html.twig', [
            'realty' => (new RealtyTransformer())->transform([$realty])[0],
            'realties_similar' => (new RealtyTransformer())->transform($realtyService->getSimilar($realty)),
            'agents' => (new AgentTransformer())->transform($agentService->getAgentOfRealty($realty)),
        ]);
    }

    public function renderSocialAction()
    {
        $socials = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Social')
            ->findAll();

        return $this->render('@App/html/_social.html.twig', [
            'socials' => $socials
        ]);
    }
    public function renderAddressesAndEmailAction()
    {
        $addresses = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Address')
            ->findAll();

        $email = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Meta')
            ->find(1);

        return $this->render('@App/html/_addresses.html.twig', [
            'addresses' => $addresses,
            'email' => $email->getText(),
        ]);
    }

    public function renderPhonesAction()
    {
        $phones = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Phone')
            ->findAll();

        return $this->render('@App/html/_phones.html.twig', [
            'phones' => $phones
        ]);
    }

    public function renderHeaderAction(bool $isShowingFilters = true)
    {
        return $this->render('@App/html/_header.html.twig', [
            'is_showing_filters' => $isShowingFilters
        ]);
    }

    public function renderFormFilterAction(Request $request, FilterService $filterService)
    {
        //TODO переписать без глобальной переменной.
        $params = (object)$_GET;

        $dealTypes = $this->getDoctrine()
            ->getRepository('AppBundle:DealType')
            ->findBy([], ['id' => 'DESC']);

        $types = $this->getDoctrine()
            ->getRepository('AppBundle:Type')
            ->findBy([], ['id' => 'ASC']);

        $districts = $this->getDoctrine()
            ->getRepository('AppBundle:District')
            ->findAll();

        $states = $this->getDoctrine()
            ->getRepository('AppBundle:State')
            ->findAll();

        $days = $this->getDoctrine()
            ->getRepository('AppBundle:Day')
            ->findBy([], ['name' => 'ASC']);

        $currencies = $this->getDoctrine()
            ->getRepository('AppBundle:Currency')
            ->findBy([], ['id' => 'DESC']);

        $livedComplexes = $this->getDoctrine()
            ->getRepository('AppBundle:LivedComplex')
            ->findBy([], ['name' => 'DESC']);

        $bathroomTypes = $this->getDoctrine()
            ->getRepository('AppBundle:BathroomType')
            ->findBy([], ['name' => 'DESC']);

        $rooms = [
            [
                'id' => 1,
                'name' => '1'
            ],
            [
                'id' => 2,
                'name' => '2'
            ],
            [
                'id' => 3,
                'name' => '3'
            ],
            [
                'id' => 4,
                'name' => '4'
            ],
            [
                'id' => 5,
                'name' => '5'
            ]
        ];

        $priceMinMax = $this->getDoctrine()->getRepository('AppBundle:Realty')->getPriceMinAndMax();
        $squareMinMax = $this->getDoctrine()->getRepository('AppBundle:Realty')->getSquareMinAndMax();

        try {
            $currency = CurrencyService::getCourse();
        } catch(\Exception $exception) {
            $currency = 0;
        }

        $subTypesCommercial = $this->getDoctrine()
            ->getRepository('AppBundle:SubType')
            ->findBy(['type' => Type::TYPES['commercial']]);

        $subTypesArea = $this->getDoctrine()
            ->getRepository('AppBundle:SubType')
            ->findBy(['type' => Type::TYPES['area']]);

        $subTypesBuilding = $this->getDoctrine()
            ->getRepository('AppBundle:SubType')
            ->findBy(['type' => Type::TYPES['building']]);

        return $this->render('@App/html/form_filter.html.twig', [
            'deal_types' => (new DefaultTransformer())->transform($dealTypes),
            'types' => (new DefaultTransformer())->transform($types),
            'districts' => (new DefaultTransformer())->transform($districts),
            'lived_complexes' => (new LivedComplexListTransformer())->transform($livedComplexes),
            'states' => (new DefaultTransformer())->transform($states),
            'days' => (new DefaultTransformer())->transform($days),
            'currencies' => (new DefaultTransformer())->transform($currencies),
            'price_from_to' => [
                'usd' => [
                    'from' => $priceMinMax['price_min'],
                    'to' => $priceMinMax['price_max']
                ],
                'uah' => [
                    'from' => $priceMinMax['price_min'] * $currency,
                    'to' => $priceMinMax['price_max'] * $currency,
                ]
            ],
            'square_from_to' => [
                'from' => $squareMinMax['square_min'],
                'to' => $squareMinMax['square_max'],
            ],
            'list_metro' => [
                'red' => (new MetroTransformer())->transform($filterService->getMetroOnLineId(1)),
                'green' => (new MetroTransformer())->transform($filterService->getMetroOnLineId(2)),
                'blue' => (new MetroTransformer())->transform($filterService->getMetroOnLineId(3)),
            ],
            'sub_types' => [
                'commercial' => $subTypesCommercial,
                'area' => $subTypesArea,
                'building' => $subTypesBuilding,
            ],
            'get' => $params,
            'rooms' => $rooms,
            'bathroomTypes' => $bathroomTypes,
            'checked_sub_type' => !empty($params->sub_type) ? (int)$params->sub_type: null,
        ]);
    }


}
