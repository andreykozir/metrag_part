<?php

// src/Metrag/AppBundle/LivedComplexController.php

namespace App\Metrag\AppBundle\Controller;

use App\Metrag\ApiBundle\Transformers\LivedComplexTransformer;
use App\Metrag\AppBundle\Entity\LivedComplexStatic;
use App\Metrag\AppBundle\Services\LivedComplexService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LivedComplexController extends Controller
{

    public function indexAction()
    {
        return $this->render('@App/lived_complex/index.html.twig');
    }

    public function showAction(LivedComplexStatic $livedComplexStatic, LivedComplexService $livedComplexService)
    {
        $renderComplexes = [];
        /** @var LivedComplexStatic $complex */
        foreach($livedComplexService->getComplexes() as $complex) {
            if($complex->getId() === $livedComplexStatic->getId()) {
                continue;
            }
            //into start or end of array
            if($complex->getMetro()->getId() === $livedComplexStatic->getMetro()->getId()) {
                array_unshift($renderComplexes, $complex);
            } else {
                $renderComplexes[] = $complex;
            }
        }

        return $this->render('@App/lived_complex/show.html.twig', [
            'lived_complex' => $livedComplexStatic,
            'lived_complexes' => (new LivedComplexTransformer)->transform($renderComplexes),
        ]);
    }

}
