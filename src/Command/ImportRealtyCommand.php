<?php
// src/Command/ImportRealtyCommand.php
namespace App\Command;

use App\Metrag\ApiBundle\Services\ImportRealtyService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportRealtyCommand extends Command
{
    private $importRealtyService;

    public function __construct(?string $name = null, ImportRealtyService $importRealtyService)
    {
        $this->importRealtyService = $importRealtyService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('app:import-realties')
            ->setDescription('Import realties')
            ->setHelp('This command allows you import realtie from metrag.com.ua');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->importRealtyService->importRealty();

        exec('php bin/console cache:clear');
    }
}
